package bsm.bikeshop.util;

import bsm.bikeshop.domain.ServiceOrderItem;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

/**
 * This class encapsulates all persistence code.
 * @author julia
 */
public class JPAUtil {
    
    private static final EntityManagerFactory FACTORY = Persistence
            .createEntityManagerFactory("bikeshop");
    
    public static EntityManager getEntityManager(){
        return FACTORY.createEntityManager();
    }

    /**
     * This method is used to insert a generic table in the database.
     * @param entity is the Entity you want to include
     */
    public static void create(Object entity) {
        EntityManager entityManager = getEntityManager();
        entityManager.getTransaction().begin();
        entityManager.persist(entity);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    public static void createOSItem(List<ServiceOrderItem> orderItems) {
        EntityManager entityManager = getEntityManager();
        entityManager.getTransaction().begin();
        orderItems.forEach(entityManager::persist);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    /**
     * This method is used to update a generic element in the database.
     * @param entity is the Entity you want to include
     */
    public static void update(Object entity) {
        EntityManager entityManager = getEntityManager();
        entityManager.getTransaction().begin();
        entityManager.merge(entity);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    /**
     * This method is used to delete a generic element in the database.
     * @param entity is the Entity you want to delete
     */
    public static void delete(Object entity) {
        EntityManager entityManager = getEntityManager();
        entityManager.getTransaction().begin();
        Object newEntity = entityManager.merge(entity);
        entityManager.remove(newEntity);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    /**
     * This method returns the Entity given an id.
     * @param id is the entity's id you are searching for.
     * @param aClass is the Class of the entity e.g. Customer.class
     * @return the generic object if it was found
     */
    public static Object findById(Integer id, Class<?> aClass) {
        EntityManager entityManager = getEntityManager();
        return entityManager.find(aClass, id);
    }

    /**
     * This method returns a List of matching elements with an exact keyword.
     * @param fieldName is the name of the field that you want to search.
     * @param keyWord is the keyword used in the search.
     * @param aClass is the Class of the entity you want to find e.g. Customer.class
     * @return List of matching elements.
     */
    public static List<?> findByKeyWord (String fieldName, String keyWord, Class<?> aClass) {
        String jpql = "SELECT o FROM " + aClass.getSimpleName() + " o WHERE o." + fieldName + " = '" + keyWord + "'";
        EntityManager entityManager = getEntityManager();
        return entityManager.createQuery(jpql, aClass).getResultList();
    }

    /**
     * This method returns a List of matching elements which contains the given keyword.
     * @param fieldName is the name of the field that you want to search.
     * @param keyWord is the keyword used in the search.
     * @param aClass is the Class of the entity you want to find e.g. Customer.class
     * @return List of matching elements.
     */
    public static List<?> findByKeyWordUsingLike (String fieldName, String keyWord, Class<?> aClass) {
        String jpql = "SELECT o FROM " + aClass.getSimpleName() + " o WHERE o." + fieldName + " like '%" + keyWord + "%'";
        EntityManager entityManager = getEntityManager();
        return entityManager.createQuery(jpql, aClass).getResultList();
    }

    /**
     * This method gets all rows in the table.
     * @param aClass is the Class of the entity you want to find e.g. Customer.class
     * @return List of all elements.
     */
    public static List<?> findAll(Class<?> aClass) {
        String jpql = "SELECT o FROM " + aClass.getSimpleName() + " o";
        EntityManager entityManager = getEntityManager();
        return entityManager.createQuery(jpql, aClass).getResultList();
    }

    public static List<?> findAllOpenOS(Class<?> aClass) {
        String jpql = "SELECT o FROM " + aClass.getSimpleName() +
                " o WHERE o.serviceOrderStatus not like 'ENTREGUE' order by o.deadline";
        EntityManager entityManager = getEntityManager();
        return entityManager.createQuery(jpql, aClass).getResultList();
    }
    
}
