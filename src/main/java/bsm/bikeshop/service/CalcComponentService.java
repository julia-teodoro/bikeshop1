package bsm.bikeshop.service;

import javax.swing.JOptionPane;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author julia
 */
public class CalcComponentService {
    
    public static Map<String,Integer> calcComponent(String height, String arm, String leg, String shoulder) {
        try {
            if (!validateFields(height, arm, leg, shoulder))
                throw new Exception("Verifique se todos os campos estão preenchidos");

            Map<String,Integer> componentsMap = new HashMap<>();

            componentsMap.put("Quadro MTB", calculateMtbFrame(height));
            componentsMap.put("Quadro ROAD", calculateRoadFrame(height));
            componentsMap.put("Pedivela", calculatePedivela(leg));
            componentsMap.put("Guidão ROAD", calculateGuidaoRoad(shoulder));
            componentsMap.put("Guidão MTB", calculateGuidaoMTB(shoulder));

            return componentsMap;

        } catch (Exception e) {
                        System.out.println(e.getMessage());
            JOptionPane.showMessageDialog(null, "ERRO: " + e.getMessage(),
                    "Bike Shop Management", JOptionPane.PLAIN_MESSAGE, null);
            return null;
        }
    }

    static private Integer calculateMtbFrame(String height) {
        int h = Integer.parseInt(height);

        if (h <= 160) return 13;
        if (h <= 170) return 15;
        if (h <= 180) return 17;
        if (h <= 190) return 19;
        return 21;
    }

    static private Integer calculateRoadFrame(String height) {
        int h = Integer.parseInt(height);

        if (h <= 155) return 46;
        if (h <= 160) return 48;
        if (h <= 165) return 50;
        if (h <= 170) return 52;
        if (h <= 175) return 54;
        if (h <= 180) return 56;
        if (h <= 185) return 58;
        if (h <= 190) return 59;
        if (h <= 195) return 60;
        return 62;
    }

    static private Integer calculateGuidaoRoad(String shoulder) {
        int s = Integer.parseInt(shoulder);

        if (s <= 38) return 38;
        if (s <= 40) return 40;
        if (s <= 42) return 42;
        return 44;
    }

    static private Integer calculateGuidaoMTB(String shoulder) {
        int s = Integer.parseInt(shoulder);
        return (s + 30) * 10;
    }

    static private Integer calculatePedivela(String leg) {
        int l = Integer.parseInt(leg);

        if (l <= 74) return 170;
        if (l <= 86) return 175;
        return 180;
    }
    
    static private boolean validateFields(String height, String arm, String leg, String shoulder) {
        if (!ValidateFieldsService.validateFieldString(height)) return false;
        if (!ValidateFieldsService.validateFieldString(arm)) return false;
        if (!ValidateFieldsService.validateFieldString(leg))  return false;
        if (!ValidateFieldsService.validateFieldString(shoulder)) return false;
        
        return true;
    }
    
}
