package bsm.bikeshop.service;

import bsm.bikeshop.domain.Bike;
import bsm.bikeshop.domain.Customer;
import bsm.bikeshop.domain.ServiceOrder;
import bsm.bikeshop.domain.ServiceOrderItem;
import bsm.bikeshop.util.JPAUtil;
import org.hibernate.PersistentObjectException;

import javax.swing.*;
import java.time.LocalDate;
import java.util.List;

public class ServiceOrderService {

    public static ServiceOrder createServiceOrder(String serviceOrderType, String serviceOrderStatus, Customer customer,
                                                  Bike bike, double totalPrice, String deadline) {
        try {
            ServiceOrder serviceOrder = new ServiceOrder(serviceOrderType, serviceOrderStatus, customer, bike,
                    "NÃO", totalPrice, ValidateFieldsService.validateFieldDeadline(deadline));
            JPAUtil.create(serviceOrder);
            return serviceOrder;

        } catch (Exception e) {
            System.out.println(e.getMessage());
            JOptionPane.showMessageDialog(null, "ERRO: " + e.getMessage(),
                    "Bike Shop Management", JOptionPane.PLAIN_MESSAGE, null);
            return null;
        }
    }

    public static void createServiceOrderItems(List<ServiceOrderItem> orderItems) {
        try {
            JPAUtil.createOSItem(orderItems);

        } catch (PersistentObjectException persistentObjectException) {
            persistentObjectException.printStackTrace();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            JOptionPane.showMessageDialog(null, "ERRO: " + e.getMessage(),
                    "Bike Shop Management", JOptionPane.PLAIN_MESSAGE, null);
        }
    }

    @SuppressWarnings("unchecked")
    public static List<ServiceOrder> findCustomersServiceOrder(Customer customer) {
        return (List<ServiceOrder>) JPAUtil.findByKeyWord("customer", String.valueOf(customer.getId()), ServiceOrder.class);
    }

    @SuppressWarnings("unchecked")
    public static List<ServiceOrder> findMaintenanceList() {
        return (List<ServiceOrder>) JPAUtil.findAllOpenOS(ServiceOrder.class);
    }

    @SuppressWarnings("unchecked")
    public static List<ServiceOrder> findBikesServiceOrder(Bike bike) {
        return (List<ServiceOrder>) JPAUtil.findByKeyWord("bike", String.valueOf(bike.getId()), ServiceOrder.class);
    }

    @SuppressWarnings("unchecked")
    public static List<ServiceOrderItem> findServiceOrdersItems(ServiceOrder s) {
        return (List<ServiceOrderItem>) JPAUtil.findByKeyWord("serviceOrder", String.valueOf(s.getId()), ServiceOrderItem.class);
    }

    public static void deleteServiceOrder(ServiceOrder s) {
        findServiceOrdersItems(s).forEach(ServiceOrderService::deleteServiceOrderItem);
        JPAUtil.delete(s);
    }

    public ServiceOrder update(ServiceOrder os) {
        os.setLastUpdated(LocalDate.now());
        JPAUtil.update(os);
        return os;
    }

    public static void deleteServiceOrderItem(ServiceOrderItem soi) {
        JPAUtil.delete(soi);
    }

}
