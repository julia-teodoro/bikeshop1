/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bsm.bikeshop.service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;


/**
 *
 * @author julia
 */
public class ValidateFieldsService {
    
    public static boolean validateFieldString(String s) {
        return s != null && !s.isEmpty() && !s.isBlank();
    }
    
    public static boolean validateCellphone(String phone) {
        return (!phone.contains(" ")) ;
    }
    
    public static LocalDate validateFieldBirthDate(String s) throws Exception {
        DateTimeFormatter df = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate date = LocalDate.parse(s, df);
        LocalDate minimum = LocalDate.of(1920,1,1);
        if (date.isAfter(LocalDate.now()) || date.isBefore(minimum) ) throw new Exception("A data informada é inválida.");
        return date;
    }
    
    public static LocalDate validateFieldDeadline(String s) throws Exception {
        DateTimeFormatter df = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate date = LocalDate.parse(s, df);
        if (date.isBefore(LocalDate.now())) throw new Exception("A data informada é inválida.");
        return date;
    }
    
    public static boolean validateFieldDouble(double d) {
        
        return false;
    }
    
}
