/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bsm.bikeshop.service;

import bsm.bikeshop.domain.Customer;
import bsm.bikeshop.util.JPAUtil;

import javax.swing.*;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.List;

/**
 *
 * @author julia
 */
public class CustomerService {
    
    public Customer createCustomer(String name, String email, String address,
                                   String date, String cellphone, String height,
                                   String arm, String leg, String shoulder) {
        try {
            Customer customer = new Customer(date, name, cellphone, address, email, height, arm, leg, shoulder);
            JPAUtil.create(customer);
            System.out.println(customer);
            return customer;

        } catch (NumberFormatException nfe) {
            System.out.println(nfe.getMessage());
            JOptionPane.showMessageDialog(null, "As medidas informadas são inválidas.",
                    "Bike Shop Management", JOptionPane.PLAIN_MESSAGE, null);
            return null;
        } catch (DateTimeParseException pe) {
            System.out.println(pe.getMessage());
            JOptionPane.showMessageDialog(null, "A data informada é inválida.", 
                    "Bike Shop Management", JOptionPane.PLAIN_MESSAGE, null);
            return null;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            JOptionPane.showMessageDialog(null, "ERRO: " + e.getMessage(),
                    "Bike Shop Management", JOptionPane.PLAIN_MESSAGE, null);
            return null;
        }
    }

    public List<?> searchCustomer(String name) {
        return JPAUtil.findByKeyWordUsingLike("name", name, Customer.class);
    }

    public Customer update(Customer customer) {
        customer.setLastUpdated(LocalDate.now());
        JPAUtil.update(customer);
        return customer;
    }

    public void delete(Customer customer) {
        JPAUtil.delete(customer);
    }

}
