package bsm.bikeshop.service;

import bsm.bikeshop.domain.Bike;
import bsm.bikeshop.domain.Customer;
import bsm.bikeshop.domain.ServiceOrder;
import bsm.bikeshop.util.JPAUtil;

import javax.swing.*;
import java.time.LocalDate;
import java.util.List;

public class BikeService {

    public static Bike createBike(String brand, String model, String color, String groupSet, Customer customer) {
        try {
            Bike bike = new Bike(color, brand, model, groupSet, customer);
            JPAUtil.create(bike);
            return bike;

        } catch (Exception e) {
            System.out.println(e.getMessage());
            JOptionPane.showMessageDialog(null, "ERRO: " + e.getMessage(),
                    "Bike Shop Management", JOptionPane.PLAIN_MESSAGE, null);
            return null;
        }    
    }

    public static Bike updateBike(Bike bike) {
        bike.setLastUpdated(LocalDate.now());
        JPAUtil.update(bike);
        return bike;
    }

    @SuppressWarnings("unchecked")
    public static List<Bike> findCustomersBikes(Customer customer) {
        return (List<Bike>) JPAUtil.findByKeyWord("customer", String.valueOf(customer.getId()), Bike.class);
    }

    public static void deleteBike(Bike b) {
        List<ServiceOrder> bikesServiceOrder = ServiceOrderService.findBikesServiceOrder(b);
        if (bikesServiceOrder != null) bikesServiceOrder.forEach(ServiceOrderService::deleteServiceOrder);
        JPAUtil.delete(b);
    }

}
