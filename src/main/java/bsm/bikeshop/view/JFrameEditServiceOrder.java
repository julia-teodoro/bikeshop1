package bsm.bikeshop.view;

import bsm.bikeshop.domain.*;
import bsm.bikeshop.service.ServiceOrderService;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.text.ParseException;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Vector;

/**
 *
 * @author julia
 */
@SuppressWarnings({"unused","FieldCanBeLocal"})
public class JFrameEditServiceOrder extends javax.swing.JFrame {

    double totalPriceOS = 0;
    ServiceOrder serviceOrder;
    Customer customer;
    Bike selectedBike;
    List<ServiceOrderItem> osItems = new ArrayList<>();

    /** Creates new form JFrameEditServiceOrder */
    public JFrameEditServiceOrder() {
        initComponents();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabelTitle = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jSeparator3 = new javax.swing.JSeparator();
        jButtonSaveOS = new javax.swing.JButton();
        jComboBoxBikeList = new javax.swing.JComboBox<>();
        jLabel4 = new javax.swing.JLabel();
        jTextFieldCustomerName = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel6 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jComboBoxType = new javax.swing.JComboBox<>();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextAreaOSDescription = new javax.swing.JTextArea();
        jLabel10 = new javax.swing.JLabel();
        jButtonDeleteItem = new javax.swing.JButton();
        jLabel11 = new javax.swing.JLabel();
        jComboBoxQuantity = new javax.swing.JComboBox<>();
        jLabel12 = new javax.swing.JLabel();
        jComboBoxStatus = new javax.swing.JComboBox<>();
        jLabel13 = new javax.swing.JLabel();
        jSeparator4 = new javax.swing.JSeparator();
        jLabel14 = new javax.swing.JLabel();
        try {
            jFormattedTextFieldDeadline = new JFormattedTextField(new javax.swing.text.MaskFormatter("##/##/####"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        jButtonInsertItem = new javax.swing.JButton();
        jFormattedTextFieldTotalPrice = new javax.swing.JFormattedTextField();
        jFormattedTextFieldUnitPrice = new javax.swing.JFormattedTextField();
        jMenuBar2 = new javax.swing.JMenuBar();
        jMenu9 = new javax.swing.JMenu();
        jMenuItem9 = new javax.swing.JMenuItem();
        jMenuItem10 = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();
        jMenuItem11 = new javax.swing.JMenuItem();
        jMenuItem12 = new javax.swing.JMenuItem();
        jMenuItem13 = new javax.swing.JMenuItem();
        jMenu4 = new javax.swing.JMenu();
        jMenuItem16 = new javax.swing.JMenuItem();
        jMenuItem17 = new javax.swing.JMenuItem();
        jMenu7 = new javax.swing.JMenu();
        jMenuItem15 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(19, 36, 56));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabelTitle.setFont(new java.awt.Font("Segoe UI", 3, 24)); // NOI18N
        jLabelTitle.setForeground(new java.awt.Color(255, 255, 255));
        jLabelTitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelTitle.setText("EDITAR ORDEM DE SERVIÇO");
        jLabelTitle.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jPanel1.add(jLabelTitle, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 800, 60));

        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Bike:");
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 100, 30, -1));

        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("ITENS DA OS");
        jPanel1.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 160, 90, -1));

        jSeparator3.setBackground(new java.awt.Color(255, 255, 255));
        jSeparator3.setForeground(new java.awt.Color(255, 255, 255));
        jPanel1.add(jSeparator3, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 530, 660, 16));

        jButtonSaveOS.setText("SALVAR");
        jButtonSaveOS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSaveOSActionPerformed(evt);
            }
        });
        jPanel1.add(jButtonSaveOS, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 570, 120, 40));

        jComboBoxBikeList.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboBoxBikeListItemStateChanged(evt);
            }
        });
        jPanel1.add(jComboBoxBikeList, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 100, 250, -1));

        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Cliente:");
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 100, 50, -1));

        jTextFieldCustomerName.setEditable(false);
        jPanel1.add(jTextFieldCustomerName, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 100, 250, -1));

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                //        {null, null, null, null},
                //        {null, null, null, null},
                //        {null, null, null, null},
                //        {null, null, null, null}
            },
            new String [] {
                "Descrição", "Quantidade", "Preço Unitário", "Preço Total"
            }
        ));
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        jPanel1.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 320, 740, 180));

        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Descrição:");
        jPanel1.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 200, 60, -1));

        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("Tipo:");
        jPanel1.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 560, 30, -1));

        jComboBoxType.setModel(new javax.swing.DefaultComboBoxModel<>(ServiceOrderType.getServiceOrderTypeList().toArray(new String[0])));
        jComboBoxType.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboBoxTypeItemStateChanged(evt);
            }
        });
        jPanel1.add(jComboBoxType, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 560, 150, -1));

        jTextAreaOSDescription.setColumns(20);
        jTextAreaOSDescription.setRows(5);
        jTextAreaOSDescription.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTextAreaOSDescriptionFocusGained(evt);
            }
        });
        jScrollPane2.setViewportView(jTextAreaOSDescription);

        jPanel1.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 200, -1, -1));

        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setText("Preço Unitário:");
        jPanel1.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 200, 80, -1));

        jButtonDeleteItem.setText("Excluir Item");
        jButtonDeleteItem.setEnabled(false);
        jButtonDeleteItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDeleteItemActionPerformed(evt);
            }
        });
        jPanel1.add(jButtonDeleteItem, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 260, 120, 30));

        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("Quantidade:");
        jPanel1.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(580, 200, 70, -1));

        jComboBoxQuantity.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" }));
        jPanel1.add(jComboBoxQuantity, new org.netbeans.lib.awtextra.AbsoluteConstraints(660, 200, -1, -1));

        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        jLabel12.setText("Status:");
        jPanel1.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 600, 40, -1));

        jComboBoxStatus.setModel(new javax.swing.DefaultComboBoxModel<>(ServiceOrderStatus.getServiceOrderStatusList().toArray(new String[0])));
        jPanel1.add(jComboBoxStatus, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 600, 150, -1));

        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setText("Valor total:");
        jPanel1.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 600, 70, -1));

        jSeparator4.setBackground(new java.awt.Color(255, 255, 255));
        jSeparator4.setForeground(new java.awt.Color(255, 255, 255));
        jPanel1.add(jSeparator4, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 170, 630, 16));

        jLabel14.setForeground(new java.awt.Color(255, 255, 255));
        jLabel14.setText("Prazo:");
        jPanel1.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 560, 50, -1));
        jPanel1.add(jFormattedTextFieldDeadline, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 560, 90, -1));

        jButtonInsertItem.setText("Incluir Item");
        jButtonInsertItem.setEnabled(false);
        jButtonInsertItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonInsertItemActionPerformed(evt);
            }
        });
        jPanel1.add(jButtonInsertItem, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 260, 120, 30));

        jFormattedTextFieldTotalPrice.setEditable(false);
        jPanel1.add(jFormattedTextFieldTotalPrice, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 600, 90, -1));
        jPanel1.add(jFormattedTextFieldUnitPrice, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 200, 90, -1));

        jMenu9.setText("Arquivo");

        jMenuItem9.setText("Voltar para a tela inicial");
        jMenuItem9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem9ActionPerformed(evt);
            }
        });
        jMenu9.add(jMenuItem9);

        jMenuItem10.setText("Fechar programa");
        jMenuItem10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem10ActionPerformed(evt);
            }
        });
        jMenu9.add(jMenuItem10);

        jMenuBar2.add(jMenu9);

        jMenu3.setText("Cadastros");

        jMenuItem11.setText("Clientes");
        jMenuItem11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem11ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem11);

        jMenuItem12.setText("Bikes");
        jMenuItem12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem12ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem12);

        jMenuItem13.setText("Nova OS");
        jMenuItem13.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem13ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem13);

        jMenuBar2.add(jMenu3);

        jMenu4.setText("Gerenciar");

        jMenuItem16.setText("Buscar Clientes");
        jMenuItem16.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem16ActionPerformed(evt);
            }
        });
        jMenu4.add(jMenuItem16);

        jMenuItem17.setText("Fila de Serviços");
        jMenuItem17.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem17ActionPerformed(evt);
            }
        });
        jMenu4.add(jMenuItem17);

        jMenuBar2.add(jMenu4);

        jMenu7.setText("Ajuda");

        jMenuItem15.setText("Sobre o Bike Shop Management");
        jMenuItem15.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem15ActionPerformed(evt);
            }
        });
        jMenu7.add(jMenuItem15);

        jMenuBar2.add(jMenu7);

        setJMenuBar(jMenuBar2);

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 651, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonSaveOSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSaveOSActionPerformed
        /**
        * todo: editar os
        * setar campos
        * salvar os
        * colocar os em cada item
        * salvar itens
        * exibir mensagem de os criada com sucesso
        * voltar para tela inicial.
        */
        try {
//            setSelectedBike();
//
//            ServiceOrder newOrder = ServiceOrderService.createServiceOrder(
//                (String) jComboBoxType.getSelectedItem(), (String) jComboBoxStatus.getSelectedItem(),
//                customer, selectedBike, totalPriceOS, jFormattedTextFieldDeadline.getText());
//            if (newOrder == null) throw new NullPointerException("Houve um erro ao tentar criar a OS");
//
//            osItems.forEach( it -> it.setServiceOrder(newOrder) );
//            ServiceOrderService.createServiceOrderItems(osItems);
//
//            JOptionPane.showMessageDialog(null, "OS num. " + newOrder.getId() + " CRIADA COM SUCESSO!!!",
//                "Bike Shop Management", JOptionPane.PLAIN_MESSAGE, null);



            this.setVisible(false);
            new bsm.bikeshop.view.JFrameMainWindow().setVisible(true);

        } catch (Exception e) {
            System.out.println(e.getMessage());
            JOptionPane.showMessageDialog(null, "ERRO: " + e.getMessage(),
                "Bike Shop Management", JOptionPane.PLAIN_MESSAGE, null);
        }
    }//GEN-LAST:event_jButtonSaveOSActionPerformed

    private void jComboBoxBikeListItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBoxBikeListItemStateChanged

    }//GEN-LAST:event_jComboBoxBikeListItemStateChanged

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        try {
            ServiceOrderItem item = getSelectedServiceOrderItem();
            setItemFields();
            jButtonDeleteItem.setEnabled(true);
        } catch (ArrayIndexOutOfBoundsException a) {
            a.printStackTrace();
            JOptionPane.showMessageDialog(null, "Selecione um Cliente.",
                "Bike Shop Management", JOptionPane.PLAIN_MESSAGE, null);
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, e.getMessage(),
                "Bike Shop Management", JOptionPane.PLAIN_MESSAGE, null);
        }
    }//GEN-LAST:event_jTable1MouseClicked

    private void jComboBoxTypeItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBoxTypeItemStateChanged
        if (jComboBoxType.getSelectedItem().equals(ServiceOrderType.ORCAMENTO)) jLabelTitle.setText("NOVO ORÇAMENTO");
        else jLabelTitle.setText("NOVA ORDEM DE SERVIÇO");
    }//GEN-LAST:event_jComboBoxTypeItemStateChanged

    private void jTextAreaOSDescriptionFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextAreaOSDescriptionFocusGained
        if (!jButtonInsertItem.isEnabled() && jComboBoxBikeList.getSelectedIndex() != -1) jButtonInsertItem.setEnabled(true);
    }//GEN-LAST:event_jTextAreaOSDescriptionFocusGained

    private void jButtonDeleteItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonDeleteItemActionPerformed
        try {
            ServiceOrderItem item = getSelectedServiceOrderItem();
            int answer = JOptionPane.showConfirmDialog(null,
                "Realmente deseja excluir este item?",
                "Bike Shop Management", JOptionPane.OK_CANCEL_OPTION);
            if (answer == JOptionPane.YES_OPTION) {
                deleteItem(item);
            }

        } catch (ArrayIndexOutOfBoundsException a) {
            a.printStackTrace();
            JOptionPane.showMessageDialog(null, "Selecione um Item.",
                "Bike Shop Management", JOptionPane.PLAIN_MESSAGE, null);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            JOptionPane.showMessageDialog(null, "ERRO: " + e.getMessage(),
                "Bike Shop Management", JOptionPane.PLAIN_MESSAGE, null);
        }
    }//GEN-LAST:event_jButtonDeleteItemActionPerformed

    private void jButtonInsertItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonInsertItemActionPerformed
        try {
            String description = jTextAreaOSDescription.getText();
            if (Objects.equals(description, "")) throw new NullPointerException("O campo descrição não pode estar vazio!");

            double unitPrice = Double.parseDouble(jFormattedTextFieldUnitPrice.getText());
            if (unitPrice <= 0) throw new NumberFormatException();

            int quantity = Integer.parseInt((String) jComboBoxQuantity.getSelectedItem());

            ServiceOrderItem item = new ServiceOrderItem(quantity, unitPrice, description);

            osItems.add(item);
            feedJTable(item);
            totalPriceOS += ( unitPrice * quantity );
            setJFormattedTextFieldTotalPrice();
            jButtonSaveOS.setEnabled(true);
            clearFields();

        } catch (NumberFormatException nfe) {
            System.out.println(nfe.getMessage());
            JOptionPane.showMessageDialog(null, "Digite o preço corretamente.",
                "Bike Shop Management", JOptionPane.PLAIN_MESSAGE, null);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            JOptionPane.showMessageDialog(null, "ERRO: " + e.getMessage(),
                "Bike Shop Management", JOptionPane.PLAIN_MESSAGE, null);
        }
    }//GEN-LAST:event_jButtonInsertItemActionPerformed

    private void jMenuItem9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem9ActionPerformed
        this.setVisible(false);
        new bsm.bikeshop.view.JFrameMainWindow().setVisible(true);
    }//GEN-LAST:event_jMenuItem9ActionPerformed

    private void jMenuItem10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem10ActionPerformed
        System.exit(0);
    }//GEN-LAST:event_jMenuItem10ActionPerformed

    private void jMenuItem11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem11ActionPerformed
        this.setVisible(false);
        new bsm.bikeshop.view.JFrameCustomer().setVisible(true);
    }//GEN-LAST:event_jMenuItem11ActionPerformed

    private void jMenuItem12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem12ActionPerformed
        try {
            this.setVisible(false);
            new bsm.bikeshop.view.JFrameBike().setVisible(true);
        } catch (ParseException pe) {
            System.out.println(pe);
        }
    }//GEN-LAST:event_jMenuItem12ActionPerformed

    private void jMenuItem13ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem13ActionPerformed
        try {
            this.setVisible(false);
            new bsm.bikeshop.view.JFrameNewServiceOrder().setVisible(true);
        } catch (ParseException pe) {
            System.out.println(pe);
        }
    }//GEN-LAST:event_jMenuItem13ActionPerformed

    private void jMenuItem16ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem16ActionPerformed
        try {
            this.setVisible(false);
            new bsm.bikeshop.view.JFrameSearchCustomer().setVisible(true);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }//GEN-LAST:event_jMenuItem16ActionPerformed

    private void jMenuItem17ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem17ActionPerformed
        JFrameMaintenanceQueue frame = new bsm.bikeshop.view.JFrameMaintenanceQueue();
        frame.serviceOrderList = ServiceOrderService.findMaintenanceList();
        frame.serviceOrderList.forEach(frame::feedJTable);
        this.setVisible(false);
        frame.setVisible(true);
    }//GEN-LAST:event_jMenuItem17ActionPerformed

    private void jMenuItem15ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem15ActionPerformed
        try {
            this.setVisible(false);
            new bsm.bikeshop.view.JFrameAbout().setVisible(true);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }//GEN-LAST:event_jMenuItem15ActionPerformed

    private void setItemFields() {
        jTextAreaOSDescription.setText((String) jTable1.getValueAt(jTable1.getSelectedRow(), 0));
        jComboBoxQuantity.setSelectedItem((String) jTable1.getValueAt(jTable1.getSelectedRow(), 1));
        jFormattedTextFieldUnitPrice.setText((String) jTable1.getValueAt(jTable1.getSelectedRow(), 2));
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public void setJTextFieldCustomerName(String name) {
        jTextFieldCustomerName.setText(name);
    }

    public void setSelectedBike(Bike selectedBike) {
        this.selectedBike = selectedBike;
    }

    public void setBike(Bike bike) {
        this.selectedBike = bike;
        jComboBoxBikeList.removeAllItems();
        jComboBoxBikeList.addItem(selectedBike.getBikeNameString());
        jComboBoxBikeList.setSelectedIndex(0);
    }

    public void setServiceOrder(ServiceOrder os) {
        this.serviceOrder = os;
    }

    public void setOsItems() {
        this.osItems = ServiceOrderService.findServiceOrdersItems(serviceOrder);
    }

    public void feedJTableOSItems() {
        this.osItems.forEach(this::feedJTable);
    }

    private ServiceOrderItem getSelectedServiceOrderItem() {
        String description = (String) jTable1.getValueAt(jTable1.getSelectedRow(), 0);
        String quantity = (String) jTable1.getValueAt(jTable1.getSelectedRow(), 1);
        String unitPrice = (String) jTable1.getValueAt(jTable1.getSelectedRow(), 2);

        return osItems.stream()
                .filter( os -> os.getDescription().equals(description)
                        && String.valueOf(os.getQuantity()).equals(quantity)
                        && String.valueOf(os.getUnitPrice()).equals(unitPrice) )
                .findFirst()
                .get();
    }

    private void clearFields() {
        jTextAreaOSDescription.setText("");
        jFormattedTextFieldUnitPrice.setText("");
        jComboBoxQuantity.setSelectedIndex(0);
    }

    public void setJFormattedTextFieldTotalPrice() {
        jFormattedTextFieldTotalPrice.setText(String.valueOf(totalPriceOS));
    }

    public void setEditionFields(ServiceOrder os) {
        setCustomer(os.getCustomer());
        setServiceOrder(os);
        setOsItems();
        setBike(os.getBike());
        setJTextFieldCustomerName(os.getCustomer().getName());
        // totalPriceOS = serviceOrder.getTotalPrice();
        setJFormattedTextFieldTotalPrice();
        jFormattedTextFieldDeadline.setText(os.getDeadline().format(DateTimeFormatter.ofPattern("ddMMyyyy")));
        jComboBoxStatus.setSelectedItem(os.getServiceOrderStatus());
        jComboBoxType.setSelectedItem(os.getServiceOrderType());
    }

    public void feedJTable(ServiceOrderItem item) {
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        Vector<String> vector = new Vector<>();
        vector.addElement(item.getDescription());
        vector.addElement(String.valueOf(item.getQuantity()));
        vector.addElement(String.valueOf(item.getUnitPrice()));
        vector.addElement(String.valueOf(item.getUnitPrice()*item.getQuantity()));
        model.addRow(vector);
    }

    private void deleteItem(ServiceOrderItem selectedItem) {
        osItems.remove(selectedItem);
        jButtonDeleteItem.setEnabled(false);
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        model.removeRow(jTable1.getSelectedRow());
        totalPriceOS = totalPriceOS - ( selectedItem.getUnitPrice() * selectedItem.getQuantity() );
        setJFormattedTextFieldTotalPrice();
        jButtonSaveOS.setEnabled(true);
        if (osItems.isEmpty()) jButtonSaveOS.setEnabled(false);
    }

    public String getTitle() {
        return "Bike Shop Management v1.0";
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JFrameEditServiceOrder.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JFrameEditServiceOrder.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JFrameEditServiceOrder.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JFrameEditServiceOrder.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new JFrameEditServiceOrder().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonDeleteItem;
    private javax.swing.JButton jButtonInsertItem;
    private javax.swing.JButton jButtonSaveOS;
    private javax.swing.JComboBox<String> jComboBoxBikeList;
    private javax.swing.JComboBox<String> jComboBoxQuantity;
    private javax.swing.JComboBox<String> jComboBoxStatus;
    private javax.swing.JComboBox<String> jComboBoxType;
    private javax.swing.JFormattedTextField jFormattedTextFieldDeadline;
    private javax.swing.JFormattedTextField jFormattedTextFieldTotalPrice;
    private javax.swing.JFormattedTextField jFormattedTextFieldUnitPrice;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabelTitle;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenu jMenu7;
    private javax.swing.JMenu jMenu9;
    private javax.swing.JMenuBar jMenuBar2;
    private javax.swing.JMenuItem jMenuItem10;
    private javax.swing.JMenuItem jMenuItem11;
    private javax.swing.JMenuItem jMenuItem12;
    private javax.swing.JMenuItem jMenuItem13;
    private javax.swing.JMenuItem jMenuItem15;
    private javax.swing.JMenuItem jMenuItem16;
    private javax.swing.JMenuItem jMenuItem17;
    private javax.swing.JMenuItem jMenuItem9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextArea jTextAreaOSDescription;
    private javax.swing.JTextField jTextFieldCustomerName;
    // End of variables declaration//GEN-END:variables
}
