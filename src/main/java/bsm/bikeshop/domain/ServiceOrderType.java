package bsm.bikeshop.domain;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author julia
 */
public class ServiceOrderType {

    public static final String VENDA = "VENDA";
    public static final String ORCAMENTO = "ORÇAMENTO";
    public static final String ORDEM_DE_SERVICO = "ORDEM DE SERVIÇO";

    public static List<String> getServiceOrderTypeList() {
        List<String> list = new ArrayList<>();
        list.add(ORDEM_DE_SERVICO);
        list.add(ORCAMENTO);
        return list;
    }
    
}
