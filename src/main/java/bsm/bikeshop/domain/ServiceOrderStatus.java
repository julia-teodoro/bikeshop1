package bsm.bikeshop.domain;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author julia
 */
public class ServiceOrderStatus {

    public static final String CRIADO = "CRIADO";
    public static final String EM_ANDAMENTO = "EM ANDAMENTO";
    public static final String AGUARDANDO_PECAS = "AGUARDANDO PEÇAS";
    public static final String AGUARDANDO_CLIENTE = "AGUARDANDO CLIENTE";
    public static final String ENTREGUE = "ENTREGUE";

    public static List<String> getServiceOrderStatusList() {
        List<String> list = new ArrayList<>();
        list.add(CRIADO);
        list.add(EM_ANDAMENTO);
        list.add(AGUARDANDO_PECAS);
        list.add(AGUARDANDO_CLIENTE);
        list.add(ENTREGUE);
        return list;
    }
    
}
