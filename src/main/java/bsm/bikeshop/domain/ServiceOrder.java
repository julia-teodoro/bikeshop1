package bsm.bikeshop.domain;

import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.*;

/**
 *
 * @author julia
 */
@Entity
@Table(name = "service_order")
public class ServiceOrder implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    
//    @Enumerated(EnumType.STRING)
    @Column(name = "service_order_type")
    private String serviceOrderType;

//    @Enumerated(EnumType.STRING)
    @Column(name = "service_order_status")
    private String serviceOrderStatus;

    @ManyToOne
    private Customer customer;
    @ManyToOne
    private Bike bike;

    @Column(name = "is_paid")
    private String isPaid;
    @Column(name = "total_price")
    private double totalPrice;
    private LocalDate deadline;
    @Column(name = "date_created")
    private LocalDate dateCreated;
    @Column(name = "last_updated")
    private LocalDate lastUpdated;

    public ServiceOrder() { }

    public ServiceOrder(String serviceOrderType, String serviceOrderStatus,
                        Customer customer, Bike bike, String isPaid, double totalPrice, LocalDate deadline) {

        this.serviceOrderType = serviceOrderType;
        this.serviceOrderStatus = serviceOrderStatus;
        this.customer = customer;
        this.bike = bike;
        this.isPaid = isPaid;
        this.totalPrice = totalPrice;
        this.deadline = deadline;
        this.dateCreated = LocalDate.now();
        this.lastUpdated = LocalDate.now();
    }

    public int getId() {
        return id;
    }

    public String getIsPaid() {
        return isPaid;
    }

    public void setIsPaid(String isPaid) {
        this.isPaid = isPaid;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public LocalDate getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(LocalDate dateCreated) {
        this.dateCreated = dateCreated;
    }

    public LocalDate getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(LocalDate lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public LocalDate getDeadline() {
        return deadline;
    }

    public void setDeadline(LocalDate deadline) {
        this.deadline = deadline;
    }

    public String getServiceOrderType() {
        return serviceOrderType;
    }

    public void setServiceOrderType(String serviceOrderType) {
        this.serviceOrderType = serviceOrderType;
    }

    public String getServiceOrderStatus() {
        return serviceOrderStatus;
    }

    public void setServiceOrderStatus(String serviceOrderStatus) {
        this.serviceOrderStatus = serviceOrderStatus;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Bike getBike() {
        return bike;
    }

    public void setBike(Bike bike) {
        this.bike = bike;
    }

}
