package bsm.bikeshop.domain;

import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.*;

/**
 *
 * @author julia
 */
@Entity
@Table(name = "service_order_item")
public class ServiceOrderItem implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private int quantity;
    @Column(name = "unit_price")
    private double unitPrice;
    private String description;
    @Column(name = "date_created")
    private LocalDate dateCreated;
    @Column(name = "last_updated")
    private LocalDate lastUpdated;
    
    @ManyToOne
    private ServiceOrder serviceOrder;

    public ServiceOrderItem() { }

    public ServiceOrderItem(int quantity, double unitPrice, String description) {
        this.quantity = quantity;
        this.unitPrice = unitPrice;
        this.description = description;
        this.dateCreated = LocalDate.now();
        this.lastUpdated = LocalDate.now();
    }

    public LocalDate getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(LocalDate dateCreated) {
        this.dateCreated = dateCreated;
    }

    public LocalDate getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(LocalDate lastUpdated) {
        this.lastUpdated = lastUpdated;
    }
    
    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getId() {
        return id;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ServiceOrder getServiceOrder() {
        return serviceOrder;
    }

    public void setServiceOrder(ServiceOrder serviceOrder) {
        this.serviceOrder = serviceOrder;
    }
    
}
