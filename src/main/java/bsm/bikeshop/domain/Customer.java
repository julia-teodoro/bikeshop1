package bsm.bikeshop.domain;

import bsm.bikeshop.service.ValidateFieldsService;

import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.*;

/**
 *
 * @author julia
 */
@Entity
public class Customer implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "birth_date")
    private LocalDate birthDate;
    private String name;
    @Column(name = "phone_number")
    private String phoneNumber;
    private String address = "não informado";
    private String email = "não informado";
    private int height;
    @Column(name = "arm_size")
    private int armSize;
    @Column(name = "leg_size")
    private int legSize;
    @Column(name = "shoulder_size")
    private int shoulderSize;
    @Column(name = "date_created")
    private LocalDate dateCreated;
    @Column(name = "last_updated")
    private LocalDate lastUpdated;

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", birthDate=" + birthDate +
                ", name='" + name + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", address='" + address + '\'' +
                ", email='" + email + '\'' +
                ", height=" + height +
                ", armSize=" + armSize +
                ", legSize=" + legSize +
                ", shoulderSize=" + shoulderSize +
                ", dateCreated=" + dateCreated +
                ", lastUpdated=" + lastUpdated +
                '}';
    }

    public Customer() { }

    public Customer(String birthDate, String name, String phoneNumber, String address, String email,
                    String height, String armSize, String legSize, String shoulderSize) throws Exception {

        if (ValidateFieldsService.validateFieldString(name)) this.name = name;
        else throw new Exception("O campo nome é obrigatório.");
        if (ValidateFieldsService.validateCellphone(phoneNumber)) this.phoneNumber = phoneNumber;
        else throw new Exception("O campo telefone é obrigatório.");

        if (ValidateFieldsService.validateFieldString(address))      this.address = address;
        if (ValidateFieldsService.validateFieldString(email))        this.email = email;
        if (ValidateFieldsService.validateFieldString(height))       this.height = Integer.parseInt(height);
        if (ValidateFieldsService.validateFieldString(armSize))      this.armSize = Integer.parseInt(armSize);
        if (ValidateFieldsService.validateFieldString(legSize))      this.legSize = Integer.parseInt(legSize);
        if (ValidateFieldsService.validateFieldString(shoulderSize)) this.shoulderSize = Integer.parseInt(shoulderSize);

        this.birthDate = ValidateFieldsService.validateFieldBirthDate(birthDate);
        this.dateCreated = LocalDate.now();
        this.lastUpdated = LocalDate.now();
    }

    public int getId() {
        return id;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phone_number) {
        this.phoneNumber = phone_number;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getArmSize() {
        return armSize;
    }

    public void setArmSize(int arm_size) {
        this.armSize = arm_size;
    }

    public int getLegSize() {
        return legSize;
    }

    public void setLeg_size(int leg_size) {
        this.legSize = leg_size;
    }

    public int getShoulderSize() {
        return shoulderSize;
    }

    public void setShoulderSize(int shoulder_size) {
        this.shoulderSize = shoulder_size;
    }
    
    public LocalDate getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(LocalDate dateCreated) {
        this.dateCreated = dateCreated;
    }

    public LocalDate getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(LocalDate lastUpdated) {
        this.lastUpdated = lastUpdated;
    }
    
}
