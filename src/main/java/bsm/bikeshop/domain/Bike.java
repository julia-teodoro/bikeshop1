package bsm.bikeshop.domain;

import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.*;

/**
 *
 * @author julia
 */
@Entity
public class Bike implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String color;
    private String brand;
    private String model;
    @Column(name = "group_set")
    private String groupSet;
    @Column(name = "date_created")
    private LocalDate dateCreated;
    @Column(name = "last_updated")
    private LocalDate lastUpdated;
    
    @ManyToOne
    private Customer customer;

    public Bike() {}

    public Bike(String color, String brand, String model, String groupSet, Customer customer) {
        this.color = color;
        this.brand = brand;
        this.model = model;
        this.groupSet = groupSet;
        this.customer = customer;
        this.lastUpdated = LocalDate.now();
        this.dateCreated = LocalDate.now();
    }

    @Override
    public String toString() {
        return "Bike{" +
                "id=" + id +
                ", color='" + color + '\'' +
                ", brand='" + brand + '\'' +
                ", model='" + model + '\'' +
                ", groupSet='" + groupSet + '\'' +
                ", dateCreated=" + dateCreated +
                ", lastUpdated=" + lastUpdated +
                ", customerName=" + customer.getName() +
                ", customerId=" + customer.getId() +
                '}';
    }

    public int getId() {
        return id;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }
    
    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getGroupSet() {
        return groupSet;
    }

    public void setGroupSet(String groupSet) {
        this.groupSet = groupSet;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public LocalDate getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(LocalDate lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public String getBikeNameString() {
        return this.brand + " " + this.model + " " + this.color + " " + this.groupSet ;
    }

}
